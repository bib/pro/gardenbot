/*********************************************************************
Sketch for Garden BOT
************************************************************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include "DHT.h"

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

byte humidity_sensor_vcc = 2;         // PIN Digital #2 to power sensors (power on when doing measure)

int pump_vcc_1 = 7;                   // PIN Digital #7 to power water pump

int sensorDHT22Pin = A0;
int sensorDHT22value = 0;

int ledPin = 13;                      // select the pin for the LED

int delay_pump_on = 3500;             // Delay to wait after activate water pump
int delay_measure_cycle = 5000;

int sensorMoisture1seuil = 70;        // Humidity desired 

int sensorMoisture1Pin = A1;          // select the input pin for Moisture sensor 1
int sensorMoisture1Value = 0;         // variable to store the value coming from the sensor 1
int sensorMoisture1ValuePercent = 0;

int sensorAmbiantLightPin = 3;         // PIN Digital #3
int sensorAmbiantLightValue = 0;       // Store ambiant light value

float AmbiantHumidityValue = 0;
float AmbiantTemperatureValue = 0;

int seuilHumidityForPump = 80;

#define DHTTYPE DHT22

#define DHTPIN 4     // what digital pin we're connected to

DHT dht(DHTPIN, DHTTYPE);

// Image soleil
const unsigned char img_sun [] PROGMEM = {
0x01, 0x80, 0x01, 0x80, 0x31, 0x0c, 0x30, 0x08, 0x07, 0xe0, 0x0e, 0x60, 0x0c, 0x30, 0xe8, 0x17, 
0xc8, 0x17, 0x0c, 0x10, 0x0e, 0x20, 0x03, 0xc0, 0x30, 0x0c, 0x21, 0x8c, 0x01, 0x80, 0x01, 0x80, 

};
 
//Byte array of bitmap of 16 x 16 px for night
   
static const unsigned char PROGMEM  img_night [] = { 
0x0, 0x20, 0x0, 0x38, 0x0, 0x3c, 0x0, 0x3e, 0x0, 0x3e, 0x0, 0x3f, 0x0, 0x7f, 0x0, 
0xff, 0x1, 0xff, 0x3, 0xff, 0xff, 0xff, 0x7f, 0xfe, 0x7f, 0xfc, 0x3f, 0xfc, 0x1f, 0xf0, 
0x7, 0xe0, 
 };

void setup()   {                
 Serial.begin(9600);

 // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
 display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64) // [i]might be the right place to put address?[/i]
 // init done
 
 //display.display(); // show splashscreen
 //delay(2000);
 display.clearDisplay();   // clears the screen and buffer


 // text display tests
 display.setTextSize(2);
 display.setTextColor(WHITE);
 display.setCursor(1,0);
 display.print("Garden");
 display.setTextColor(BLACK, WHITE); // 'inverted' text
 display.println("BOT");
 display.setTextSize(1);
 display.setTextColor(WHITE);
 display.print("Starting "); 
 delay(500); 
 // declare the ledPin as an OUTPUT:
 pinMode(ledPin, OUTPUT);

 // Init the humidity sensor board
  pinMode(humidity_sensor_vcc, OUTPUT);
  digitalWrite(humidity_sensor_vcc, LOW);

    // Init the relay pump 1
  pinMode(pump_vcc_1, OUTPUT);
  digitalWrite(pump_vcc_1, LOW);

  display.print(".");
  display.display();
  delay(500); 
  // Init the humidity sensor board
  pinMode(sensorAmbiantLightPin, INPUT);
  
  display.print(".");
  display.display(); 
  delay(500); 
  
  dht.begin();
  display.print("."); 
  display.display();
  delay(1500);

  display.clearDisplay();   // clears the screen and buffer
 

}

void turnPumpOn(int pump_pin_number) {
  digitalWrite(pump_pin_number,LOW);
}

void turnPumpOff(int pump_pin_number) {
  digitalWrite(pump_pin_number,HIGH);
}

int read_humidity_sensor(int humidity_sensor_pin) {
  digitalWrite(humidity_sensor_vcc, HIGH);
  delay(500);
  int value = analogRead(humidity_sensor_pin);
  digitalWrite(humidity_sensor_vcc, LOW);
  return 1023 - value;
}


// Reading temperature or humidity takes about 250 milliseconds!
float read_AmbianTemperature() {
  // Read temperature as Celsius (the default)
  float AmbiantTemperatureValue = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(AmbiantTemperatureValue)) {
    Serial.println("Failed to read from DHT sensor!");
    return 0;
  }
  Serial.print("Ambiant temperature : ");
  Serial.println(AmbiantTemperatureValue);
  
  return AmbiantTemperatureValue;
}

// Reading temperature or humidity takes about 250 milliseconds!
// Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
float read_AmbiantHumidity() {
  
  float AmbiantHumidityValue = dht.readHumidity();

  // Check if any reads failed and exit early (to try again).
  if (isnan(AmbiantHumidityValue)) {
    Serial.println("Failed to read from DHT sensor!");
    return 0;
  }
  Serial.print("Ambiant humidity : ");
  Serial.println(AmbiantHumidityValue);
  
  return AmbiantHumidityValue;
}


int read_ambiantlight_sensor() {
  return digitalRead(sensorAmbiantLightPin);
}

int convert_into_percent(int sensorValue) {
  return map(sensorValue,0,1023,0,100);
}

void loop() {

  display.clearDisplay();   // clears the screen and buffer
  display.setCursor(0,0);

  display.drawLine(0, 21, 128, 21, WHITE);
  display.drawLine(45, 23, 45, 32, WHITE);

  /* 
   * read the value from the moisture sensor 1 
   */
  display.print("Hum1 : ");
  sensorMoisture1Value = read_humidity_sensor(sensorMoisture1Pin);
  sensorMoisture1ValuePercent = convert_into_percent(sensorMoisture1Value);
  display.print(sensorMoisture1ValuePercent);
  display.print("% / ");
  display.println(sensorMoisture1seuil);

  /* 
   * Ambiant light 
   */
  sensorAmbiantLightValue = read_ambiantlight_sensor();
  
  // Too much light : sun is shining
  if(sensorAmbiantLightValue == 0) {
    // miniature bitmap display
    display.drawBitmap(110, 2,  img_sun, 16, 16, 1);
    delay(1);
  } else {
    // miniature bitmap display
    display.drawBitmap(110, 2,  img_night, 16, 16, 1);
    delay(1);
  }
  
  /*
   * Ambiant temperature
   */
  display.setCursor(0,24);
  display.print("Ambiant ");
  display.print(read_AmbianTemperature());
  display.print("C");
  //display.display();

  /*
   * Ambiant humidity
   */
  display.print(" ");
  display.print(read_AmbiantHumidity());
  display.print("%");
  display.display();


  /*
   * Pump management
   * 
   * Si la luminosité indique la nuit 
   * et si l'humidité ambiante est inférieur au seuil voulu (on arrose pas quand il pleut!)
   * et si la valeur du capteur d'humidité est inférieure au seuil voulu pour la plante.
   */
  if( (sensorAmbiantLightValue == 1) 
    && (AmbiantHumidityValue < seuilHumidityForPump)
    && (sensorMoisture1ValuePercent < sensorMoisture1seuil) ) {
    turnPumpOn(pump_vcc_1);
    delay(delay_pump_on);
    Serial.println("Turn pump ON !!");
  }
  else {
    turnPumpOff(pump_vcc_1);
    Serial.println("Turn pump OFF !!");
  }

  Serial.print("Moisture : ");
  Serial.print(sensorMoisture1ValuePercent);
  Serial.print("/");
  Serial.println(sensorMoisture1seuil);
  Serial.println("END                      ---");
  
  
  
  // Wait a moment before to start again
  delay(delay_measure_cycle); 
} // END LOOP



void testdrawchar(void) {
 display.setTextSize(1);
 display.setTextColor(WHITE);
 display.setCursor(0,0);

 for (uint8_t i=0; i < 168; i++) {
   if (i == '\n') continue;
   display.write(i);
   if ((i > 0) && (i % 21 == 0))
     display.println();
 }    
 display.display();
}



